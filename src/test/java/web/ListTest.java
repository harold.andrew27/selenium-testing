package web;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

import model.ListLocators;

public class ListTest extends ListLocators {

	@BeforeClass()
	public static void beforeSetup() {
		setup();
	}
	
	@Test()
	public void sortPriceDesceding() {
		
		System.out.println("\nTEST: Sorting Price Descending");

		int listCount = 1;
		double price1 = 0.00;
		double price2 = 0.00;
		
		waitAndClick(sortByXpath);
		driver.findElement(By.xpath(priceDescendingXpath)).click();
		threadSleep(3000);
		
		while(isItemsDisplayed(listCount, listCount+1)) {
			
			System.out.println("Comparing items #'s "+listCount+" and "+(listCount+1));
			
			price1 = Double.parseDouble(driver.findElement(By.xpath(
					"//*[@id=\"content-listings\"]/ul[2]/li["+listCount+"]/div[2]/p[1]/span")).getText().replaceAll("\\D+",""));
			price2 = Double.parseDouble(driver.findElement(By.xpath(
					"//*[@id=\"content-listings\"]/ul[2]/li["+(listCount+1)+"]/div[2]/p[1]/span")).getText().replaceAll("\\D+",""));
			
			Assert.assertTrue(price1 > price2);
			
			listCount++;
		}
	}
	
	@Test()
	public void sortPriceAscending() {
		
		System.out.println("\nTEST: Sorting Price Ascending");

		int listCount = 1;
		double price1 = 0.00;
		double price2 = 0.00;
		
		waitAndClick(sortByXpath);
		driver.findElement(By.xpath(priceAscendingXpath)).click();
		threadSleep(3000);
		
		while(isItemsDisplayed(listCount, listCount+1)) {
			
			System.out.println("Comparing items #'s "+listCount+" and "+(listCount+1));
			
			price1 = Double.parseDouble(driver.findElement(By.xpath(
					"//*[@id=\"content-listings\"]/ul[2]/li["+listCount+"]/div[2]/p[1]/span")).getText().replaceAll("\\D+",""));
			price2 = Double.parseDouble(driver.findElement(By.xpath(
					"//*[@id=\"content-listings\"]/ul[2]/li["+(listCount+1)+"]/div[2]/p[1]/span")).getText().replaceAll("\\D+",""));
			
			Assert.assertTrue(price1 < price2);
			
			listCount++;
		}
	}
	
	@Test
	public void sortByAtoZ() {
		
		System.out.println("\nTEST: Sorting A to Z");
		
		int listCount = 1;
		String title1 = "";
		String title2 = "";
		
		waitAndClick(sortByXpath);
		driver.findElement(By.xpath(aToZXpath)).click();
		threadSleep(3000);
		
		while(isItemsDisplayed(listCount, listCount+1)) {
			
			System.out.println("Comparing items #'s "+listCount+" and "+(listCount+1));
			
			title1 = driver.findElement(By.xpath("//*[@id=\"content-listings\"]/ul[2]/li["+listCount+"]/div[2]/span/a")).getText();
			title2 = driver.findElement(By.xpath("//*[@id=\"content-listings\"]/ul[2]/li["+(listCount+1)+"]/div[2]/span/a")).getText();
			
			Assert.assertTrue(title1.compareToIgnoreCase(title2) <= 0);
			
			listCount++;
		}
	}
	
	@Test
	public void sortByZtoA() {
		
		System.out.println("\nTEST: Sorting Z to A");
		
		int listCount = 1;
		String title1 = "";
		String title2 = "";
		
		waitAndClick(sortByXpath);
		driver.findElement(By.xpath(zToAXpath)).click();
		threadSleep(3000);
		
		while(isItemsDisplayed(listCount, listCount+1)) {
			
			System.out.println("Comparing items #'s "+listCount+" and "+(listCount+1));
			
			title1 = driver.findElement(By.xpath("//*[@id=\"content-listings\"]/ul[2]/li["+listCount+"]/div[2]/span/a")).getText();
			title2 = driver.findElement(By.xpath("//*[@id=\"content-listings\"]/ul[2]/li["+(listCount+1)+"]/div[2]/span/a")).getText();
			
			Assert.assertTrue(title1.compareToIgnoreCase(title2) >= 0);
			
			listCount++;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void checkImage300pxWide() {
		
		System.out.println("\nTEST: Check Image 300px Wide");
		
		int listCount = 1;
		String width = "";
		List<Boolean> checks = new ArrayList();
		
		threadSleep(5000);
		while(!driver.findElements(By.xpath("//*[@id=\"content-listings\"]/ul[2]/li["+listCount+"]")).isEmpty()) {
			
			width = driver.findElement(By.xpath(
					"//*[@id=\"content-listings\"]/ul[2]/li["+listCount+"]/div[1]/a/img[1]")).getAttribute("naturalWidth");
			
			checks.add(width.equals("300"));
			
			System.out.println("Item #"+listCount+" has "+width+"px.");
			
			listCount++;
		}
		
		Assert.assertFalse(checks.contains(false));
		
	}
	
	public boolean isItemsDisplayed(int listCount1, int listCount2) {
		if(!driver.findElements(By.xpath(
				"//*[@id=\"content-listings\"]/ul[2]/li["+listCount1+"]")).isEmpty() &&
				!driver.findElements(By.xpath(
						"//*[@id=\"content-listings\"]/ul[2]/li["+listCount2+"]")).isEmpty()
				) {
			
			return true;
		}
		
		return false;
	}

}
