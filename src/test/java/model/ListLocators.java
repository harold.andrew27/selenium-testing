package model;

import setup.InitialSetup;

public class ListLocators extends InitialSetup {

	public static String sortByXpath = "//*[@id=\"content-listings\"]/div[1]/div[2]/select";
	public static String priceDescendingXpath = "//*[@id=\"content-listings\"]/div[1]/div[2]/select/option[2]";
	public static String priceAscendingXpath = "//*[@id=\"content-listings\"]/div[1]/div[2]/select/option[3]";
	public static String aToZXpath = "//*[@id=\"content-listings\"]/div[1]/div[2]/select/option[5]";
	public static String zToAXpath = "//*[@id=\"content-listings\"]/div[1]/div[2]/select/option[6]";
	
}
