package setup;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class InitialSetup {
	
	public static WebDriver driver;
	
	public static void threadSleep(int millis) {
		driver.manage().timeouts().implicitlyWait(millis, TimeUnit.MILLISECONDS);
	}
	
	public static void waitAndClick(String xpath) {
		while(driver.findElements(By.xpath(xpath)).isEmpty()) {
			threadSleep(500);
		}
		driver.findElement(By.xpath(xpath)).click();
	}

	public static void setup() {
		
		//setup ChromeDriver
		System.setProperty("webdriver.chrome.driver", "chromedriver\\chromedriver.exe");
		driver = new ChromeDriver();
		
		//maximize browser
		driver.manage().window().maximize();
		
		//initialize URL
		driver.get("https://ap-empire.agentimage.com/properties/");
		
		threadSleep(10000);
	}
}
