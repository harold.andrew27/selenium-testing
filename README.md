# Selenium Testing

Automated testing for QA Engineer Exam

Setup:
-Install JDK
    -Download latest JDK(JDK 13)
    -Install downloaded JDK
    -From Computer,
        -right click and select Properties
        -click Advanced system settings
        -go to Advance tab, then click Environment Variables
            -from User Variables,
                -click New
                    -Variable Name: JAVA_HOME
                    -Variable Value: jdk path from your PC(C:\Program Files\Java\jdk-13.0.2)
            -from System Variables,
                -select variable "Path" then click edit
                    -at Variable Value field,
                        -copy your jdk bin folder from your PC(C:\Program Files\Java\jdk-13.0.2\bin) and concatenate it at the end with semi-colon(;)
    -Verify JDK
        -open cmd
        -type "java -version"
        -it should show the java version

-Install Eclipse
-Install Chrome

From Eclipse:
-go to Window >> Show View >> Other >> Type "git" >> Select "Git Repositories" then click Open
-Click on "Clone a Git Repository and add the clone to this view"
-paste "https://gitlab.com/harold.andrew27/selenium-testing" on URI field then click Next 
-select "master" then click Next
-click Finish
-right click on the cloned repository and click "Import Projects"
-click Finish

-from your Project Explorer,
   -Open ListTest.java from src/test/java/web
   -right click >> Run As >> JUnit Test
   -automated test will now be running